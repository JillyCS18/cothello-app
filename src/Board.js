import React from 'react'
import Square from './Square'
import {
  adversarialSearch,
  createArray64,
  findPossibleMove,
  flipDisks,
  possibleMoveExist,
  printvar,
  incSpread,
} from './evaluation'

/*
 * a = Antibody / player
 * v = Virus / AI
 */

import 'materialize-css'
import { Button, Icon, Modal } from 'react-materialize'

const difficulties = {
  EASY: 2,
  MEDIUM: 6,
  HARD: 8,
  EXTREME: 60,
}

class Board extends React.Component {
  constructor(props) {
    super(props)
    const board = createArray64(null)
    board[3][3] = 'v'
    board[3][4] = 'a'
    board[4][3] = 'a'
    board[4][4] = 'v'

    // debug
    //board = [
    //  [null, null, null, null, null, 'v', null, 'v'],
    //  [null, null, null, null, null, 'v', null, 'v'],
    //  [null, null, null, null, null, 'v', null, 'v'],
    //  [null, null, null, null, null, 'v', null, 'v'],
    //  [null, null, null, null, null, 'v', null, 'v'],
    //  [null, null, null, null, null, 'a', null, 'v'],
    //  [null, null, null, null, null, 'v', 'v', null],
    //  [null, null, null, null, null, 'v', null, null],
    //]

    this.state = {
      board: board,
      aIsNext: true,
      possibleMove: createArray64(null),
      gameStarted: false,
      difficulty: 0,
      turnCounter: 0,
      timerTime: 0,
      numOfA: 2,
      numOfV: 2,
      gamePaused: false,
      tempBoard: board,
      tempPossibleMove: createArray64(null),
      gameOver: false,
      automated: false,
      isSpreading: false,
      virusTripleMoveCount: 0,
      gameModeSelected: false,
    }
  }

  componentDidMount() {
    const possibleMove = findPossibleMove(
      'a',
      this.state.board,
      0,
      this.state.isSpreading
    )
    this.setState({ possibleMove: possibleMove })
  }

  handleClick(y, x) {
    if (
      this.state.board[y][x] === null &&
      this.state.possibleMove[y][x] !== null &&
      this.state.aIsNext
    ) {
      const newBoard = flipDisks(
        this.state.board,
        this.state.possibleMove[y][x],
        'a'
      )
      console.log('BOARD', this.state.turnCounter)
      //console.log(printBoard(newBoard))
      this.countTiles(newBoard)
      if (this.state.numOfA + this.state.numOfV === 64) {
        this.setState({
          gameOver: true,
        })
      }
      const newPossibleMove = findPossibleMove(
        'v',
        newBoard,
        this.state.virusTripleMoveCount,
        this.state.isSpreading
      )

      // if virus has no move, skip virus move
      if (!possibleMoveExist(newPossibleMove)) {
        const newPossibleMovePlayer = findPossibleMove(
          'a',
          newBoard,
          this.state.virusTripleMoveCount
        )

        // if both player has no move
        if (!possibleMoveExist(newPossibleMovePlayer)) {
          this.setState({
            board: newBoard,
            gameOver: true,
          })
        } else {
          this.setState({
            board: newBoard,
            aIsNext: true,
            possibleMove: newPossibleMovePlayer,
            turnCounter: this.state.turnCounter + 1,
          })
        }
        return
      }

      this.setState(
        {
          board: newBoard,
          aIsNext: false,
          possibleMove: newPossibleMove,
          turnCounter: this.state.turnCounter + 1,
        },
        () => this.moveAI(newBoard, newPossibleMove)
      )
    }
  }

  moveAI(newBoard, newPossibleMove) {
    // let testms = 0
    // let testinterval = setInterval(() => {
    //   testms++
    //   console.log(testms)
    //   // console.log(ms)
    //   // this.setState({
    //   //   timerTime: ms,
    //   // })
    // }, 1)
    // console.log("mahdia coba print")
    // clearInterval(testinterval)

    setTimeout(() => {
      // console.log('woww')
      // console.log('triple counter' + this.state.virusTripleMoveCount)
      // let ms = 0
      // let interval = setInterval(() => {
      //   console.log("waktu nih")
      //   ms++
      //   console.log(ms)
      //   this.setState({
      //     timerTime: ms,
      //   })
      // }, 1)
      const start = Date.now()
      const { x, y } = adversarialSearch(
        newBoard,
        this.state.difficulty,
        'v',
        this.state.virusTripleMoveCount,
        this.state.isSpreading
      )
      const time = Date.now() - start
      console.log('waktu', time)
      printvar()
      // clearInterval(interval)
      // console.log('woww2', x, y, value, newPossibleMove)
      let newBoard2 = flipDisks(newBoard, newPossibleMove[y][x], 'v')
      this.countTiles(newBoard2)
      this.setState({ timerTime: time })

      // console.log("board before spread", newBoard2)

      // Kalo dia move ke 3
      // if (this.state.isSpreading && this.state.virusTripleMoveCount === 0) {
      //   console.log("SPREADDD ")
      //   newBoard2 = spreadVirus(newBoard2)
      // const { board, x, y, value } = spreadVirus(newBoard2a)
      // this.countTiles(board)
      // const newBoard2b = board
      // }

      // const newBoard2 = (this.state.isSpreading && this.state.virusTripleMoveCount === 0) ? newBoard2b : newBoard2a

      // if player has no move, skip player move
      const newPossibleMovePlayer = findPossibleMove(
        'a',
        newBoard2,
        this.state.virusTripleMoveCount,
        this.state.isSpreading
      )
      if (!possibleMoveExist(newPossibleMovePlayer)) {
        const newPossibleMoveAI = findPossibleMove(
          'v',
          newBoard2,
          this.state.virusTripleMoveCount,
          this.state.isSpreading
        )

        // if both player has no move
        if (!possibleMoveExist(newPossibleMoveAI)) {
          this.setState({
            board: newBoard2,
            gameOver: true,
          })
          // if AI can move again
        } else {
          this.setState(
            {
              board: newBoard2,
              aIsNext: false,
              possibleMove: newPossibleMoveAI,
              turnCounter: this.state.turnCounter + 1,
              virusTripleMoveCount: incSpread(this.state.virusTripleMoveCount),
            },
            () => this.moveAI(newBoard2, newPossibleMoveAI)
          )
        }
        return
      }

      // board is full
      if (this.state.numOfA + this.state.numOfV === 64) {
        this.setState({
          board: newBoard2,
          gameOver: true,
        })
      }
      // game paused
      if (this.state.gamePaused) {
        this.setState({
          tempBoard: newBoard2,
          tempPossibleMove: findPossibleMove(
            'a',
            newBoard2,
            this.state.virusTripleMoveCount,
            this.state.isSpreading
          ),
        })
        // normal ai move
      } else {
        this.setState({
          board: newBoard2,
          aIsNext: true,
          possibleMove: findPossibleMove(
            'a',
            newBoard2,
            this.state.virusTripleMoveCount,
            this.state.isSpreading
          ),
          virusTripleMoveCount: incSpread(this.state.virusTripleMoveCount),
          turnCounter: this.state.turnCounter + 1,
        })
      }
    }, 1000)
  }

  handleDifficultyChoice(event) {
    const difficultyValue =
      event.target.innerText === 'EASY'
        ? difficulties.EASY
        : event.target.innerText === 'MEDIUM'
        ? difficulties.MEDIUM
        : event.target.innerText === 'HARD'
        ? difficulties.HARD
        : event.target.innerText === 'EXTREME'
        ? difficulties.EXTREME
        : 0

    this.setState({
      difficulty: difficultyValue,
    })
  }

  renderBoard = () => {
    let content = []
    for (let y = 0; y < 8; y++) {
      let row = []
      for (let x = 0; x < 8; x++) {
        row.push(
          <Square
            key={x}
            // title={x + ',' + y}
            value={this.state.board[y][x]}
            possible={this.state.possibleMove[y][x] !== null}
            vTurn={!this.state.aIsNext}
            onClick={() => this.handleClick(y, x)}
          />
        )
      }
      content.push(
        <div key={y} className="board-row">
          {row}
        </div>
      )
    }
    return content
  }

  pauseOrResume = () => {
    if (this.state.gamePaused) {
      this.setState({
        board: this.state.tempBoard,
        aIsNext: true,
        possibleMove: this.state.tempPossibleMove,
        turnCounter: this.state.turnCounter + 1,
      })
    }
    this.setState({
      gamePaused: !this.state.gamePaused,
    })
  }

  restart = () => {
    const temp = createArray64(null)
    temp[3][3] = 'v'
    temp[3][4] = 'a'
    temp[4][3] = 'a'
    temp[4][4] = 'v'
    this.setState(
      {
        board: temp,
        aIsNext: true,
        gameStarted: false,
        turnCounter: 0,
        gamePaused: false,
        gameOver: false,
        automated: false,
        numOfA: 2,
        numOfV: 2,
        virusTripleMoveCount: 0,
        gameModeSelected: false,
        difficulty: 0,
      },
      () => {
        const possibleMove = findPossibleMove(
          'a',
          this.state.board,
          0,
          this.state.isSpreading
        )
        this.setState({
          possibleMove: possibleMove,
        })
      }
    )
  }

  automate = () => {
    const {
      board,
      possibleMove,
      numOfA,
      numOfV,
      turnCounter,
      gamePaused,
      difficulty,
    } = this.state
    var newBoard2 = board
    var newPossibleMove2 = possibleMove
    var newBoard, newPossibleMove
    for (let i = 0; i < 60; i++) {
      const { x, y } = adversarialSearch(
        newBoard2,
        difficulty,
        'a',
        this.state.virusTripleMoveCount,
        this.state.isSpreading
      )
      newBoard = flipDisks(newBoard2, newPossibleMove2[y][x], 'a')
      newPossibleMove = findPossibleMove(
        'v',
        newBoard,
        this.state.virusTripleMoveCount,
        this.state.isSpreading
      )
      this.countTiles(newBoard)
      if (numOfA + numOfV === 64) {
        this.setState({
          gameOver: true,
        })
      }
      this.setState(
        {
          board: newBoard,
          aIsNext: false,
          possibleMove: newPossibleMove,
          turnCounter: turnCounter + 1,
        },
        // eslint-disable-next-line
        () => {
          setTimeout(() => {
            const { x, y } = adversarialSearch(
              newBoard,
              difficulty,
              'v',
              this.state.virusTripleMoveCount,
              this.state.isSpreading
            )
            newBoard2 = flipDisks(newBoard, newPossibleMove[y][x], 'v')
            this.countTiles(newBoard2)
            newPossibleMove2 = findPossibleMove(
              'a',
              newBoard2,
              this.state.virusTripleMoveCount,
              this.state.isSpreading
            )
            if (numOfA + numOfV === 64) {
              this.setState({
                gameOver: true,
              })
            }
            if (gamePaused) {
              this.setState({
                tempBoard: newBoard2,
                tempPossibleMove: newPossibleMove2,
              })
            } else {
              this.setState({
                board: newBoard2,
                possibleMove: newPossibleMove2,
                aIsNext: true,
                turnCounter: turnCounter + 1,
              })
            }
          }, 1000)
        }
      )
    }
  }

  countTiles = (currentBoard) => {
    let v = 0
    let a = 0

    for (let y = 0; y < 8; y++) {
      for (let x = 0; x < 8; x++) {
        if (currentBoard[x][y] === 'a') {
          a++
        } else if (currentBoard[x][y] === 'v') {
          v++
        }
      }
    }

    this.setState({ numOfV: v, numOfA: a })
  }

  render() {
    const {
      difficulty,
      gameStarted,
      timerTime,
      aIsNext,
      gameOver,
      numOfA,
      numOfV,
      gamePaused,
      automated,
      gameModeSelected,
      isSpreading,
    } = this.state

    // let centiseconds = ("0" + (Math.floor(timerTime) % 100)).slice(-2);
    // let seconds = ("0" + (Math.floor(timerTime / 1000) % 60)).slice(-2);
    // let minutes = ("0" + (Math.floor(timerTime / 60000) % 60)).slice(-2);
    // let hours = ("0" + Math.floor(timerTime / 3600000)).slice(-2);
    const status = 'Next player: ' + (this.state.aIsNext ? 'You' : 'AI')

    return (
      <div
        style={{
          display: `flex`,
          flexDirection: `column`,
          alignItems: `center`,
          justifyContent: `center`,
        }}
      >
        <h1 style={{ marginBottom: 0, marginTop: 0 }}>COTHELLO</h1>
        <h5 style={{ marginBottom: `3%` }}>
          A classic othello game with a unique twist!
          <a
            href="https://gitlab.com/JillyCS18/cothello-app"
            rel="noopener noreferrer"
            target="_blank"
            style={{
              fontSize: 14,
              display: `flex`,
              justifySelf: `center`,
              alignSelf: `center`,
            }}
          >
            Visit Repository
          </a>
        </h5>
        {gameStarted ? <></> : <h5 style={{ marginBottom: 0 }}>Game Mode</h5>}
        <div
          style={{
            display: `flex`,
            flexDirection: `row`,
            alignItems: `center`,
            justifyContent: `center`,
            marginTop: `3%`,
          }}
        >
          <Button
            node="a"
            tooltip="The original othello (reversi) experience."
            className={
              !gameStarted && gameModeSelected && !isSpreading
                ? 'green'
                : 'blue accent-3'
            }
            style={{
              marginRight: '5px',
            }}
            onClick={
              gameStarted
                ? null
                : () =>
                    this.setState({
                      isSpreading: false,
                      gameModeSelected: true,
                    })
            }
            disabled={
              gameStarted && this.state.isSpreading && !(difficulty === 0)
                ? true
                : false
            }
          >
            Classic
          </Button>
          <Button
            node="a"
            tooltip="Our original take on an interesting othello game mechanic."
            className={
              !gameStarted && gameModeSelected && isSpreading
                ? 'green'
                : 'blue accent-3'
            }
            style={{
              marginRight: '5px',
            }}
            onClick={
              gameStarted
                ? null
                : () =>
                    this.setState({ isSpreading: true, gameModeSelected: true })
            }
            disabled={
              gameStarted && !this.state.isSpreading && !(difficulty === 0)
                ? true
                : false
            }
          >
            Cothello
          </Button>
        </div>
        {gameStarted ? (
          <></>
        ) : (
          <h5 style={{ marginBottom: 0, marginTop: `5%` }}>AI Difficulty</h5>
        )}
        <div
          style={{
            display: `flex`,
            flexDirection: `row`,
            alignItems: `center`,
            justifyContent: `center`,
            marginTop: `3%`,
          }}
        >
          <Button
            node="a"
            tooltip="The AI isn't very bright, but it can still beat you if you're not careful."
            className={
              !gameStarted &&
              gameModeSelected &&
              difficulty === difficulties.EASY
                ? 'green'
                : 'blue accent-3'
            }
            style={{
              marginRight: '5px',
            }}
            onClick={
              gameStarted ? null : (event) => this.handleDifficultyChoice(event)
            }
            disabled={
              gameModeSelected
                ? gameStarted
                  ? difficulty === difficulties.EASY
                    ? false
                    : true
                  : false
                : true
            }
          >
            EASY
          </Button>
          <Button
            node="a"
            tooltip="A more advanced AI. Might take a bit of effort to win."
            className={
              !gameStarted &&
              gameModeSelected &&
              difficulty === difficulties.MEDIUM
                ? 'green'
                : 'blue accent-3'
            }
            style={{
              marginRight: '5px',
            }}
            onClick={
              gameStarted ? null : (event) => this.handleDifficultyChoice(event)
            }
            disabled={
              gameModeSelected
                ? gameStarted
                  ? difficulty === difficulties.MEDIUM
                    ? false
                    : true
                  : false
                : true
            }
          >
            MEDIUM
          </Button>
          <Button
            node="a"
            tooltip="This AI is significantly more clever than the other options. Prepare to be beaten!"
            className={
              !gameStarted &&
              gameModeSelected &&
              difficulty === difficulties.HARD
                ? 'green'
                : 'blue accent-3'
            }
            style={{
              marginRight: '5px',
            }}
            onClick={
              gameStarted ? null : (event) => this.handleDifficultyChoice(event)
            }
            disabled={
              gameModeSelected
                ? gameStarted
                  ? difficulty === difficulties.HARD
                    ? false
                    : true
                  : false
                : true
            }
          >
            HARD
          </Button>
          <Button
            node="a"
            tooltip="For research purposes only! You have been warned..."
            className={
              !gameStarted &&
              gameModeSelected &&
              difficulty === difficulties.EXTREME
                ? 'green'
                : 'blue accent-3'
            }
            onClick={
              gameStarted ? null : (event) => this.handleDifficultyChoice(event)
            }
            disabled={
              gameModeSelected
                ? gameStarted
                  ? difficulty === difficulties.EXTREME
                    ? false
                    : true
                  : false
                : true
            }
          >
            EXTREME
          </Button>
        </div>
        {gameStarted ? (
          <div
            style={{
              marginTop: `7%`,
              display: `flex`,
              flexDirection: `row`,
              width: `100%`,
              justifyContent: `center`,
              alignItems: `center`,
            }}
          >
            <h6>
              <b>Antibody Score</b>&emsp;
              <b style={{ color: `cyan` }}>{numOfA}</b>&emsp;(You)
            </h6>
            <div
              style={{
                width:
                  numOfA.toString().length > 1 && numOfV.toString().length > 1
                    ? `10%`
                    : numOfA.toString().length > 1 ||
                      numOfV.toString().length > 1
                    ? `13%`
                    : `15%`,
              }}
            />
            <h6>
              <b>Virus Score</b>&emsp;
              <b style={{ color: `tomato` }}>{numOfV}</b>&emsp;(AI)
            </h6>
          </div>
        ) : (
          <div style={{ marginTop: `10%` }} />
        )}
        {gameStarted ? (
          <div
            style={{
              marginBottom: `2%`,
            }}
          >
            {this.renderBoard()}
          </div>
        ) : (
          <Button
            className="blue accent-3"
            tooltip="Start the game!"
            tooltipOptions={{
              position: 'top',
            }}
            onClick={() => this.setState({ gameStarted: true })}
            disabled={gameModeSelected && !(difficulty === 0) ? false : true}
          >
            Play
            <Icon left>play_arrow</Icon>
          </Button>
        )}
        {/* <div
          style={{
            marginBottom: `2%`,
            opacity: gameStarted ? `1` : `0.5`,
            pointerEvents: gameStarted ? `` : `none`,
          }}
        >
          {this.renderBoard()}
        </div> */}
        {gameStarted ? (
          <>
            {this.state.turnCounter > 0 ? (
              this.state.turnCounter > 0 && !aIsNext ? (
                <h6>AI is making a move</h6>
              ) : (
                <h6>AI took {timerTime}ms to make a move</h6>
              )
            ) : (
              <h6>AI is watching</h6>
            )}
            {gameOver ? (
              <h5 className="status" style={{ marginTop: `1%` }}>
                Game Over!
              </h5>
            ) : (
              <h5 className="status" style={{ marginTop: `1%` }}>
                {status}
              </h5>
            )}
          </>
        ) : (
          <></>
        )}
        {gameStarted ? (
          <div
            style={{
              display: `flex`,
              flexDirection: `row`,
              justifyContent: `center`,
              alignItems: `center`,
              marginTop: `2%`,
              width: `100%`,
            }}
          >
            <Button
              className="blue accent-3"
              tooltip="Pause the game to see the AI's possible moves."
              tooltipOptions={{
                position: 'top',
              }}
              style={{
                marginRight: `5%`,
              }}
              onClick={() => this.pauseOrResume()}
              disabled={aIsNext}
            >
              {gamePaused ? `Resume` : `Pause`}
              <Icon left>{gamePaused ? `play_arrow` : `pause`}</Icon>
            </Button>
            <Button
              className="blue accent-3"
              tooltip="Reset the game state to the start. You can choose a different difficulty."
              tooltipOptions={{
                position: 'top',
              }}
              style={{
                marginRight: `5%`,
              }}
              onClick={() => this.restart()}
            >
              Restart
              <Icon left>replay</Icon>
            </Button>
            <Button
              className="blue accent-3"
              tooltip="Let an AI take your place. It's a fair fight between AI vs AI!"
              tooltipOptions={{
                position: 'top',
              }}
              onClick={() => this.automate()}
              style={{
                backgroundColor: automated ? `tomato` : `#2979FF`,
              }}
            >
              {automated ? `Stop` : `Automate`}
              <Icon left>{automated ? `stop` : `loop`}</Icon>
            </Button>
          </div>
        ) : (
          <></>
        )}
        <Modal
          actions={[
            <Button flat modal="close" node="button" waves="green">
              Close
            </Button>,
          ]}
          bottomSheet={false}
          fixedFooter={false}
          header={
            numOfA > numOfV
              ? `You Win!`
              : numOfA === numOfV
              ? `It's A Draw!`
              : `You Lose!`
          }
          open={gameOver}
          options={{
            dismissible: true,
            endingTop: '10%',
            inDuration: 250,
            onCloseEnd: null,
            onCloseStart: null,
            onOpenEnd: null,
            onOpenStart: null,
            opacity: 0.5,
            outDuration: 250,
            preventScrolling: true,
            startingTop: '4%',
          }}
        >
          <p>
            {numOfA > numOfV
              ? `Congratulations on beating the AI!`
              : numOfA === numOfV
              ? `Wow, both of you managed to get a tie! It seems the AI's intellect is on par with yours!`
              : `Too bad... it seems the AI won this time.`}
          </p>
        </Modal>
      </div>
    )
  }
}

//// DEBUG function
//function printBoard(boardState) {
//  let row = ''
//  for (let y = 0; y < 8; y++) {
//    for (let x = 0; x < 8; x++) {
//      row += ' ' + (boardState[y][x] === null ? '-' : boardState[y][x]) + ' '
//    }
//    row += '\n'
//  }
//  return row
//}

export default Board
