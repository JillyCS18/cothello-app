export const SPREAD_NUM = 3

export function incSpread(x) {
  if (x !== -1) {
    return (x + 1) % SPREAD_NUM
  }
  return -1
}

let pruneCounter = 0
const debugAdv = false
const debutMd = 5
export function adversarialSearch(
  boardState,
  depth,
  player,
  tripleCounter,
  isSpreading,
  alpha = -Infinity,
  beta = Infinity
) {
  // base case
  const possibleMove = findPossibleMove(
    player,
    boardState,
    tripleCounter,
    isSpreading
  )
  if (depth === 0) {
    if (debugAdv && depth >= debutMd)
      console.log({ type: 'final', value: evaluate(boardState) })
    return {
      x: -1,
      y: -1,
      value: evaluate(boardState),
      alpha: -Infinity,
      beta: Infinity,
    }
  }
  const opPlayer = player === 'a' ? 'v' : 'a'
  let bestMove

  if (player === 'v') {
    if (debugAdv && depth >= 5) console.group('v ' + depth)
    let maxEval = -Infinity
    const possibleMovesCoor = possibleMoves(boardState, possibleMove, player)
    if (possibleMovesCoor.length === 0) {
      return adversarialSearch(
        boardState,
        depth - 1,
        player,
        incSpread(tripleCounter),
        isSpreading,
        alpha,
        beta
      )
    } else {
      for (let i = 0; i < possibleMovesCoor.length; i++) {
        const newBoard = flipDisks(
          boardState,
          possibleMove[possibleMovesCoor[i][0]][possibleMovesCoor[i][1]],
          player
        )
        const res = adversarialSearch(
          newBoard,
          depth - 1,
          opPlayer,
          incSpread(tripleCounter),
          isSpreading,
          alpha,
          beta
        )
        if (res.value > maxEval) {
          maxEval = res.value
          bestMove = [possibleMovesCoor[i][1], possibleMovesCoor[i][0]]
        }
        if (beta <= alpha) {
          if (debugAdv && depth >= debutMd)
            console.log('prune', possibleMovesCoor.length - i - 1)
          pruneCounter += possibleMovesCoor.length - i - 1
          return {
            x: bestMove[0],
            y: bestMove[1],
            value: maxEval,
            alpha: alpha,
            beta: beta,
          }
        }
        alpha = Math.max(alpha, res.value)
      }
    }
    if (debugAdv && depth >= debutMd)
      console.log({
        type: 'v',
        x: bestMove[0],
        y: bestMove[1],
        value: maxEval,
        depth: depth,
        alpha: alpha,
        beta: beta,
      })
    if (debugAdv && depth >= debutMd) console.groupEnd()
    return {
      x: bestMove[0],
      y: bestMove[1],
      value: maxEval,
      alpha: alpha,
      beta: beta,
    }
  } else {
    if (debugAdv && depth >= debutMd) console.group('a ' + depth)
    let minEval = +Infinity
    const possibleMovesCoor = possibleMoves(boardState, possibleMove, player)
    let bestMove
    if (possibleMovesCoor.length === 0) {
      return adversarialSearch(
        boardState,
        depth - 1,
        player,
        incSpread(tripleCounter),
        isSpreading,
        alpha,
        beta
      )
    } else {
      for (let i = 0; i < possibleMovesCoor.length; i++) {
        const newBoard = flipDisks(
          boardState,
          possibleMove[possibleMovesCoor[i][0]][possibleMovesCoor[i][1]],
          player
        )
        const res = adversarialSearch(
          newBoard,
          depth - 1,
          opPlayer,
          incSpread(tripleCounter),
          isSpreading,
          alpha,
          beta
        )
        if (res.value < minEval) {
          minEval = res.value
          bestMove = [possibleMovesCoor[i][1], possibleMovesCoor[i][0]]
        }
        if (beta <= alpha) {
          if (debugAdv && depth >= debutMd)
            console.log('prune', possibleMovesCoor.length - i - 1)
          pruneCounter += possibleMovesCoor.length - i - 1
          return {
            x: bestMove[0],
            y: bestMove[1],
            value: minEval,
            depth: depth,
            alpha: alpha,
            beta: beta,
          }
        }
        beta = Math.min(beta, res.value)
      }
    }
    if (debugAdv && depth >= debutMd)
      console.log({
        type: 'a',
        x: bestMove[0],
        y: bestMove[1],
        value: minEval,
        depth: depth,
        alpha: alpha,
        beta: beta,
      })
    if (debugAdv && depth >= debutMd) console.groupEnd()
    return {
      x: bestMove[0],
      y: bestMove[1],
      value: minEval,
      depth: depth,
      alpha: alpha,
      beta: beta,
    }
  }
}

export function printvar() {
  console.log('prunecounter', pruneCounter)
  pruneCounter = 0
}

function possibleMoves(boardState, possibleMove, player) {
  const possibleMovesCoor = []
  for (let y = 0; y < 8; y++) {
    for (let x = 0; x < 8; x++) {
      if (possibleMove[y][x] !== null) {
        possibleMovesCoor.push([y, x])
      }
    }
  }
  return possibleMovesCoor
}

export function createArray64(filler) {
  const temp = Array(8)
  for (let x = 0; x < 8; x++) {
    temp[x] = Array(8).fill(filler)
  }
  return temp
}

export function evaluate(boardState) {
  let total = 0

  const weight = [
    [10, 3, 6, 6, 6, 6, 3, 10],
    [3, 3, 1, 1, 1, 1, 3, 3],
    [6, 1, 1, 1, 1, 1, 1, 6],
    [6, 1, 1, 1, 1, 1, 1, 6],
    [6, 1, 1, 1, 1, 1, 1, 6],
    [6, 1, 1, 1, 1, 1, 1, 6],
    [3, 3, 1, 1, 1, 1, 3, 3],
    [10, 3, 6, 6, 6, 6, 3, 10],
  ]

  for (let x = 0; x < 8; x++) {
    for (let y = 0; y < 8; y++) {
      let z
      if (boardState[y][x] === 'a') z = -1
      else if (boardState[y][x] === 'v') z = 1
      else z = 0

      total += weight[y][x] * z
    }
  }
  return total
}

export function findPossibleMove(
  player,
  boardState,
  tripleCounter,
  isSpreading
) {
  // if spread counter hits zero
  if (isSpreading && player === 'v' && tripleCounter === 2) {
    return findPossibleSpread(boardState)
  }

  const possibleMove = createArray64(null)

  function getSquare(x, y) {
    if (x < 0 || x >= 8 || y < 0 || y >= 8) return null
    return boardState[y][x]
  }

  const opPlayer = player === 'a' ? 'v' : 'a'
  // for every square
  for (let x = 0; x < 8; x++) {
    for (let y = 0; y < 8; y++) {
      // skip if not empty
      if (getSquare(x, y) !== null) {
        continue
      }

      let flippedDisks = new Set()

      const DIRECTIONS = [
        [-1, -1], // top-left
        [-1, 0], // left
        [-1, 1], // bot-left
        [0, -1], // top
        [0, 1], // bot
        [1, -1], // top-right
        [1, 0], // right
        [1, 1], // bot-right
      ]
      DIRECTIONS.forEach((dir) => {
        // if next to it is enemy
        if (getSquare(x + dir[0], y + dir[1]) === opPlayer) {
          let dirFlippedDisks = []
          dirFlippedDisks.push(10 * (x + dir[0]) + (y + dir[1])) // location (4, 7) = 47

          let cx = x + 2 * dir[0]
          let cy = y + 2 * dir[1]

          // dijepit player
          while (getSquare(cx, cy) === opPlayer) {
            dirFlippedDisks.push(10 * cx + cy)
            cx += dir[0]
            cy += dir[1]
          }

          if (getSquare(cx, cy) === player) {
            dirFlippedDisks.forEach((item) => flippedDisks.add(item))
          }
        }
      })

      if (flippedDisks.size > 0) {
        flippedDisks.add(x * 10 + y)
        //console.log(x + ',' + y, flippedDisks) // debug
        possibleMove[y][x] = flippedDisks
      }
    }
  }
  //console.log('======')
  return possibleMove
}

export function flipDisks(boardState, disks, player) {
  const newBoardState = boardState.map((arr) => arr.map((r) => r))
  disks.forEach((num) => {
    const x = Math.floor(num / 10)
    const y = num % 10
    newBoardState[y][x] =
      newBoardState[y][x] === null
        ? player
        : newBoardState[y][x] === 'a'
        ? 'v'
        : 'a'
  })
  return newBoardState
}

export function possibleMoveExist(possibleMove) {
  for (let i = 0; i < 8; i++) {
    for (let j = 0; j < 8; j++) {
      if (possibleMove[i][j] !== null) {
        return true
      }
    }
  }
  return false
}

export function findPossibleSpread(boardState) {
  const possibleSpread = createArray64(null)

  function getSquare(x, y) {
    if (x < 0 || x >= 8 || y < 0 || y >= 8) return null
    return boardState[y][x]
  }

  const opPlayer = 'a'
  // for every square
  for (let x = 0; x < 8; x++) {
    for (let y = 0; y < 8; y++) {
      // skip if not 'v'
      if (getSquare(x, y) !== 'v') {
        continue
      }

      let infectedDisks = new Set()

      const DIRECTIONS = [
        [-1, 0], // left
        [0, -1], // top
        [0, 1], // bot
        [1, 0], // right
      ]
      DIRECTIONS.forEach((dir) => {
        // if next to it is enemy
        if (getSquare(x + dir[0], y + dir[1]) === opPlayer) {
          infectedDisks.add(10 * (x + dir[0]) + (y + dir[1])) // location (4, 7) = 47
        }
      })

      if (infectedDisks.size > 0) {
        //console.log(x + ',' + y, flippedDisks) // debug
        possibleSpread[y][x] = infectedDisks
      }
    }
  }
  //console.log('======')
  return possibleSpread
}

export function infectDisks(boardState, disks) {
  const newBoardState = boardState.map((arr) => arr.map((r) => r))
  disks.forEach((num) => {
    const x = Math.floor(num / 10)
    const y = num % 10
    newBoardState[y][x] = 'v'
  })
  return newBoardState
}
