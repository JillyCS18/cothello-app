import React from 'react'
import Virus from './img/virus.png'
import WhiteCell from './img/white-blood-cell.png'

import 'materialize-css'
import { Button } from 'react-materialize'

class Square extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      value: null,
    }
  }

  render() {
    return (
      <Button
        className={
          'square ' +
          (this.props.possible
            ? this.props.vTurn
              ? 'opponent'
              : 'possible'
            : '')
        }
        onClick={() => this.props.onClick()}
        style={{
          borderRadius: `10%`,
          margin: `3px`,
          width: `50px`,
          height: `50px`,
          border: `none`,
        }}
      >
        {this.props.value === null ? null : this.props.value === 'a' ? (
          <img
            src={WhiteCell}
            alt="A"
            style={{
              maxHeight: `40px`,
              maxWidth: `40px`,
              marginTop: `8%`,
              marginLeft: `3%`,
            }}
          />
        ) : (
          <img
            src={Virus}
            alt="V"
            style={{
              maxHeight: `40px`,
              maxWidth: `40px`,
              marginTop: `8%`,
              marginLeft: `1%`,
            }}
          />
        )}
        <span
          style={{
            fontSize: 10,
            verticalAlign: `middle`,
            display: `flex`,
            justifyContent: `center`,
            color: 'blue',
          }}
        >
          {this.props.title}
        </span>
      </Button>
    )
  }
}

export default Square
